import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  form = new FormGroup({});
  model = { email: '', password: '' };
  options: FormlyFormOptions = {};

  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        label: 'Email',
        placeholder: 'Enter email',
        pattern: /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
        required: true,
      },
       validation: {
        messages: {
          required:'This field is required',
          pattern: (error, field: FormlyFieldConfig) => `"${field.formControl?.value}" is not a valid email`,
        },
      },
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        type: 'password',
        label: 'Password',
        placeholder: 'Enter password',
        required: true,
        minLength: 6,
      },
       validation: {
        messages: {
          required:'This field is required',
          minlength: (error, field: FormlyFieldConfig) => `Your password need "${field.templateOptions?.minLength}" characters`,
        },
      },
    },
  ];

  constructor(public auth: AuthService) {}

  ngOnInit(): void {}

  onSubmit(model: any) {
    if (model.email != '' && model.password != '' && this.form.valid) {
      this.auth.register(model);
      return;
    }
  }
}
