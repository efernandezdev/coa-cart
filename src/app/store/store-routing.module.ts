import { CartComponent } from './pages/cart/cart.component';
import { NgModule } from '@angular/core';
import { ProductsComponent } from './pages/products/products.component';
import { RouterModule, Routes } from '@angular/router';
import { StoreComponent } from './store.component';

const routes: Routes = [
  {
    path: '',
    component: StoreComponent,
    children: [
      { path: 'cart', component: CartComponent },
      { path: 'store', component: ProductsComponent },
      { path: '', redirectTo: '/store', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StoreRoutingModule {}
