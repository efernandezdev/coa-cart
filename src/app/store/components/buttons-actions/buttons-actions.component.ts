import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';
import { CartService } from 'src/app/services/cart.service';

export interface MyCellParams {
  cartPage?: boolean;
}

@Component({
  selector: 'app-buttons-actions',
  templateUrl: './buttons-actions.component.html',
  styleUrls: ['./buttons-actions.component.css'],
})
export class ButtonsActionsComponent implements ICellRendererAngularComp {
  public cartPage: boolean = false;
  private dataCell: any;

  constructor(private cart: CartService, public dialog: MatDialog) {}

  refresh(params: ICellRendererParams): boolean {
    return false;
  }
  agInit(params: ICellRendererParams & MyCellParams): void {
    this.dataCell = params.node.data;
    this.cartPage = params.cartPage ?? false;
  }

  addProductCart() {
    if (!localStorage.getItem('cartId')) {
      this.dialog.open(DialogContent);
      return;
    }
    this.cart.addItem(this.dataCell);
  }

  deleteProductCart() {
    this.cart.removeProduct(this.dataCell.cartProductsId);
  }

  subtractProductCart() {
    this.cart.subtracstItem(this.dataCell.cartProductsId);
  }
}
@Component({
  selector: 'dialog-content',
  template: ` Please  <a href="/login">Login</a> or <a href="/register">Register</a> `,
})
export class DialogContent {}
