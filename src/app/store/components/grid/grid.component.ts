import { Component, Input, OnInit } from '@angular/core';
import { ColDef, FirstDataRenderedEvent} from 'ag-grid-community';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css'],
})
export class GridComponent implements OnInit {
  @Input() data: any;
  @Input() column!: ColDef[];
  constructor() {}

  ngOnInit(): void {}

  public defaultColDef: ColDef = {
    resizable: true,
  };

   onFirstDataRendered(params: FirstDataRenderedEvent) {
    params.api.sizeColumnsToFit();
  }
}
