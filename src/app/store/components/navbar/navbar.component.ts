import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit,OnDestroy {
  quantity = 0;
  subscription!: Subscription;
  constructor(public authservice: AuthService, private cart: CartService) {
    if (localStorage.getItem('cartId')) {
      this.subscription = this.cart.getItemsCart().subscribe((products) => {
        let items = [...products];
        this.quantity = items.reduce(
          (acc: any, ac: any) => (acc += ac.quantity),
          0
        );
      });
    }
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit(): void {}

  logout() {
    this.subscription.unsubscribe();
    this.quantity = 0;
    this.authservice.logout();
  }
}
