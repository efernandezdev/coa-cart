import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css'],
})
export class TotalComponent implements OnInit, OnDestroy {
  private subscription!: Subscription;
  total: number = 0;

  constructor(private cart: CartService, private router: Router) {
    if (localStorage.getItem('cartId')) {
      this.subscription = this.cart.getItemsCart().subscribe((products) => {
        let items = [...products];
        this.total = items.reduce(
          (acc: any, ac: any) => (acc += ac.quantity * ac.price),
          0
        );
      });
    }
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  goToBill() {
    this.router.navigate(['bill']);
  }

  ngOnInit(): void {}
}
