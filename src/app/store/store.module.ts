import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
//Own Components
import { BillComponent } from './pages/bill/bill.component';
import { ButtonsActionsComponent } from './components/buttons-actions/buttons-actions.component';
import { CartComponent } from './pages/cart/cart.component';
import { GridComponent } from './components/grid/grid.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProductsComponent } from './pages/products/products.component';
import { StoreComponent } from './store.component';
import { TotalComponent } from './components/total/total.component';
//Others Modules
import { AgGridModule } from 'ag-grid-angular';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
@NgModule({
  declarations: [
    BillComponent,
    ButtonsActionsComponent,
    CartComponent,
    GridComponent,
    NavbarComponent,
    ProductsComponent,
    StoreComponent,
    TotalComponent,
  ],
  imports: [
    AgGridModule.withComponents([]),
    CommonModule,
    MatBadgeModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatDividerModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    RouterModule,
  ],
})
export class StoreModule {}
