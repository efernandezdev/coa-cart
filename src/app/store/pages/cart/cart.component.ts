import { Component, OnDestroy, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { CartService } from 'src/app/services/cart.service';
import { ButtonsActionsComponent } from '../../components/buttons-actions/buttons-actions.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit, OnDestroy {
  subscription!: Subscription;
  public rowData: any;
  public columnDefs: ColDef[] = [
    { field: 'name', sortable: true },
    { field: 'description', sortable: true },
    { field: 'price', sortable: true },
    {
      field: 'quantity',
      editable: true,
      type: 'numericColumn',
      singleClickEdit: true,
      valueParser: (params) =>
        this.cart.updateItem(params.newValue, params.data),
    },
    {
      headerName: 'Actions',
      cellRenderer: ButtonsActionsComponent,
      cellRendererParams: {
        cartPage: true,
      },
      minWidth: 170,
    },
  ];

  constructor(private cart: CartService) {
    if (localStorage.getItem('cartId')) {
      this.subscription = this.cart.getItemsCart().subscribe((products) => {
        this.rowData = products;
      });
    }
    this.rowData = [];
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  ngOnInit(): void {}
}
