import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProductCart } from 'src/app/interface/product-cart';
import { AuthService } from 'src/app/services/auth.service';
import { BillService } from 'src/app/services/bill.service';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css'],
})
export class BillComponent implements OnInit, OnDestroy {
  private subscription!: Subscription;
  total: number = 0;
  displayedColumns: string[] = ['quantity', 'desc', 'cost', 'total'];
  products: Array<ProductCart> = [];

  constructor(private bill: BillService, private auth: AuthService) {
    if (localStorage.getItem('cartId')) {
      this.subscription = this.bill.getItemsCart().subscribe((products) => {
        this.products = products;
        let items = [...products];
        this.total = items.reduce(
          (acc: any, ac: any) => (acc += ac.quantity * ac.price),
          0
        );
      });
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit(): void {}

  buy() {
    let uid = this.auth.uid;
    this.bill.buyCart(uid);
  }
}
