import { Component, OnDestroy, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { ProductsService } from 'src/app/services/products.service';
import { ButtonsActionsComponent } from '../../components/buttons-actions/buttons-actions.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit, OnDestroy {
  subscription!: Subscription;
  public rowData: any;
  public columnDefs: ColDef[] = [
    { field: 'name', sortable: true },
    { field: 'description', sortable: true },
    { field: 'price', sortable: true },
    {
      headerName: 'Action',
      cellRenderer: ButtonsActionsComponent,
    },
  ];

  constructor(private productsservices: ProductsService) {}

  ngOnInit(): void {
    this.subscription = this.productsservices
      .getProducts()
      .subscribe((product) => {
        this.rowData = product;
      });
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
