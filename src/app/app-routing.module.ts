import { AuthRoutingModule } from './auth/auth-routing.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreRoutingModule } from './store/store-routing.module';
import { BillComponent } from './store/pages/bill/bill.component';

const routes: Routes = [
  { path: 'bill', component: BillComponent },
  { path: 'register', redirectTo: '/register', pathMatch: 'full' },
  { path: 'login', redirectTo: '/login', pathMatch: 'full' },
  { path: '', redirectTo: '/store', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    StoreRoutingModule,
    AuthRoutingModule,
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
