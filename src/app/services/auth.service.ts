import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Cart } from '../interface/cart';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public uid: any;
  constructor(
    private auth: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore
  ) {
    this.auth.onAuthStateChanged((authId) => {
      return (this.uid = authId?.uid);
    });
  }

  register(data: { email: string; password: string }) {
    this.auth
      .createUserWithEmailAndPassword(data.email, data.password)
      .then((uid) => {
        this.db
          .collection('carts')
          .add({
            id: '',
            userId: uid.user?.uid,
            status: { completed: false, pending: true },
          })
          .then((docRef) => {
            localStorage.setItem('cartId', JSON.stringify(docRef.id));
            this.db
              .collection('carts')
              .doc(docRef.id)
              .update({
                id: docRef.id,
              })
              .then(() => {
                this.router.navigateByUrl('/store');
              });
          });
        return;
      });
  }

  login(data: { email: string; password: string }) {
    this.auth
      .signInWithEmailAndPassword(data.email, data.password)
      .then((uid) => {
        this.db
          .collection('carts', (ref) =>
            ref
              .where('userId', '==', uid.user?.uid)
              .where('status.completed', '==', false)
          )
          .get()
          .subscribe((a) => {
            if (a.docs.length) {
              return a.docs
                .map((ac) => {
                  return ac.data() as Cart;
                })
                .map((q) => {
                  localStorage.setItem('cartId', JSON.stringify(q.id));
                  this.router.navigateByUrl('/store');
                });
            } else {
              this.db
                .collection('carts')
                .add({
                  id: '',
                  userId: uid.user?.uid,
                  status: { completed: false, pending: true },
                })
                .then((docRef) => {
                  localStorage.setItem('cartId', JSON.stringify(docRef.id));
                  this.db
                    .collection('carts')
                    .doc(docRef.id)
                    .update({
                      id: docRef.id,
                    })
                    .then(() => {
                      this.router.navigateByUrl('/store');
                    });
                });
              return;
            }
          });
        return;
      });
  }

  logout() {
    localStorage.clear();
    this.auth.signOut();
    this.router.navigateByUrl('/store');
  }

  logged() {
    return this.uid;
  }
}
