import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Product } from '../interface/product';
import { ProductCart } from '../interface/product-cart';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  constructor(private db: AngularFirestore) {}

  addItem(data: Product) {
    let id = JSON.parse(localStorage.getItem('cartId') || '');
    this.db
      .collection('carts')
      .doc(id)
      .collection('products', (ref) => ref.where('id', '==', data.id))
      .get()
      .subscribe((results) => {
        if (results.docs.length) {
          results.docs
            .map((p) => {
              return p.data() as ProductCart;
            })
            .map((q) => {
              this.db
                .collection('carts')
                .doc(id)
                .collection('products')
                .doc(q.cartProductsId)
                .update({
                  quantity: q.quantity + 1,
                });
            });
        } else {
          data.quantity = 1;
          this.db
            .collection('carts')
            .doc(id)
            .collection('products')
            .add(data)
            .then((docRef) => {
              this.db
                .collection('carts')
                .doc(id)
                .collection('products')
                .doc(docRef.id)
                .update({
                  cartProductsId: docRef.id,
                });
            });
        }
      });
  }

  removeProduct(dataId: string) {
    let id = JSON.parse(localStorage.getItem('cartId') || '');
    this.db
      .collection('carts')
      .doc(id)
      .collection('products')
      .doc(dataId)
      .delete();
  }

  subtracstItem(dataId: Product) {
    let id = JSON.parse(localStorage.getItem('cartId') || '');
    this.db
      .collection('carts')
      .doc(id)
      .collection('products', (ref) =>
        ref.where('cartProductsId', '==', dataId)
      )
      .get()
      .subscribe((a) => {
        a.docs
          .map((ac) => {
            return ac.data() as ProductCart;
          })
          .map((q) => {
            if (q.quantity > 1) {
              this.db
                .collection('carts')
                .doc(id)
                .collection('products')
                .doc(q.cartProductsId)
                .update({ quantity: q.quantity - 1 });
            } else {
              this.removeProduct(q.cartProductsId);
            }
          });
      });
  }

  updateItem(quantity: string, data: ProductCart) {
    let id = JSON.parse(localStorage.getItem('cartId') || '');
    if (Number(quantity) > 0) {
      this.db
        .collection('carts')
        .doc(id)
        .collection('products')
        .doc(data.cartProductsId)
        .update({
          quantity: Number(quantity),
        });
    } else {
      if (Number(quantity) == 0) {
        this.removeProduct(data.cartProductsId);
      } else {
        return data.quantity;
      }
    }
    return Number(quantity);
  }

  getItemsCart() {
    let id = JSON.parse(localStorage.getItem('cartId') || '');

    return this.db
      .collection('carts')
      .doc(id)
      .collection('products')
      .snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((a) => {
            const data = a.payload.doc.data() as ProductCart;
            return data;
          });
        })
      );
  }
}
