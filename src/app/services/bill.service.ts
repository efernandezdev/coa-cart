import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ProductCart } from '../interface/product-cart';

@Injectable({
  providedIn: 'root',
})
export class BillService {
  constructor(private db: AngularFirestore, private router: Router) {}

  getItemsCart() {
    let id = JSON.parse(localStorage.getItem('cartId') || '');

    return this.db
      .collection('carts')
      .doc(id)
      .collection('products')
      .snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((a) => {
            const data = a.payload.doc.data() as ProductCart;
            return data;
          });
        })
      );
  }

  async buyCart(uid: string) {
    let id = JSON.parse(localStorage.getItem('cartId') || '');

    await this.db
      .collection('carts')
      .doc(id)
      .update({status: {completed: true, pending: false}});
    this.db
      .collection('carts')
      .add({
        id: '',
        userId: uid,
        status: {completed: false, pending: true},
      })
      .then((docRef) => {
        localStorage.setItem('cartId', JSON.stringify(docRef.id));
        this.db
          .collection('carts')
          .doc(docRef.id)
          .update({
            id: docRef.id,
          })
          .then(() => {
            this.router.navigate(['/store']);
          });
      });
  }
}
