export interface ProductCart {
  description: string;
  id: string;
  name: string;
  price: number;
  quantity: number;
  cartProductsId: string;
}
