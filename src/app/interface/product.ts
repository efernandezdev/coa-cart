export interface Product {
  description: string;
  id: string;
  name: string;
  price: number;
  quantity?: number;
  cartProductsId?: string;
}
