export interface Cart {
  id?: string;
  userId?: string;
  status?: { completed: boolean; pending: boolean };
}
