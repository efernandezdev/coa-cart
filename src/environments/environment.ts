// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCmh9gp2GZwHMGONjnt0G8-owHy29b8sJw',
    authDomain: 'coa-store-c70b0.firebaseapp.com',
    projectId: 'coa-store-c70b0',
    storageBucket: 'coa-store-c70b0.appspot.com',
    messagingSenderId: '461421012017',
    appId: '1:461421012017:web:ae4215d6b7d22a4e2a6110',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
